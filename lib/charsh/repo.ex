defmodule Charsh.Repo do
  use Ecto.Repo,
    otp_app: :charsh,
    adapter: Ecto.Adapters.Postgres
end
