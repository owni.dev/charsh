defmodule CharshWeb.PageHTML do
  use CharshWeb, :html

  embed_templates "page_html/*"
end
