defmodule CharshWeb.Layouts do
  use CharshWeb, :html

  embed_templates "layouts/*"
end
